export default {
  // projectRoot: '.',     // Where to resolve all URLs relative to. Useful if you have a monorepo project.
  pages: './src/pages',
  dist: './dist',
  public: './public',
  buildOptions: {
    site: 'http://mikevalstar.com',
    sitemap: true,
  },
  devOptions: {
    // port: 3000,         // The port to run the dev server on.
    tailwindConfig: './tailwind.config.js',
  },
  renderers: ['@astrojs/renderer-react'],
};
