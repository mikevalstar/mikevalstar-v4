var DOMReady = callback => {
  document.readyState === 'interactive' || document.readyState === 'complete' ? callback() : document.addEventListener('DOMContentLoaded', callback);
};

var showTheme = () => {
  if (localStorage.theme === 'dark' || !('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches) {
    document.documentElement.classList.add('dark');
  } else {
    document.documentElement.classList.remove('dark');
  }
};

DOMReady(() => {

  showTheme();

  const toggle = document.getElementById('themeToggle');

  if(toggle) {
    toggle.innerHTML = localStorage.theme === 'dark' ? '<i class="fal fa-sun fa-2x"></i>' : '<i class="fas fa-moon-cloud fa-2x"></i>';
    toggle.onclick = () => {
      localStorage.theme = localStorage.theme === 'dark' ? 'light' : 'dark';
      toggle.innerHTML = localStorage.theme === 'dark' ? '<i class="fal fa-sun fa-2x"></i>' : '<i class="fas fa-moon-cloud fa-2x"></i>';
      showTheme();
    };
  }
});
