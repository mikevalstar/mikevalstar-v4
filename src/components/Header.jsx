import React from 'react';

const Header = props => {

  const {title} = props;

  return <><nav className='border-solid border-gray-400 border-b'>
    <div className='py-3 bg-gradient-to-r from-indigo-600 to-light-blue-500'>
      <div className='container mx-auto px-4'>
        <button id='themeToggle' className='float-right'><i className='fas fa-moon-cloud fa-2x'></i></button>
        <h1 className='text-2xl font-bold mb-0'>{title || 'Mike Valstar\'s Thoughts'}</h1>
      </div>
    </div>
    <div className='container max-w-5xl mx-auto px-4 py-4'>
      <ul className='flex'>
        <li className='mr-6'><a className='link' href='/'>Home</a></li>
        <li className='mr-6'><a className='link' href='/posts/1/'>All Posts</a></li>
        <li className='mr-6'><a className='link' href='/about'>About</a></li>
        <li className='mr-6'><a className='link' href='/projects'>Projects</a></li>
      </ul>
    </div>
  </nav></>;
};

export default Header;
