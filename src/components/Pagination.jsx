import React from 'react';

const Pagination = props => {

  const {prevUrl, nextUrl} = props;

  return <div className='px-4 py-4'>
    <nav className='max-w-5xl mx-auto'>
      {prevUrl ? <a className='link' href={prevUrl} aria-label='Previous Page'>Prev</a> : ''}
      {nextUrl ? <a className='link float-right' href={nextUrl} aria-label='Next Page'>Next</a> : ''}
    </nav>
  </div>;
};

export default Pagination;

