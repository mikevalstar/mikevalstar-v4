import React from 'react';

function formatDate(date) {
  return new Date(date).toUTCString().replace(/(\d\d\d\d) .*/, '$1'); // remove everything after YYYY
}

const PostExcerpt = props => {

  const {post} = props;

  return <article className='px-4 py-4'>
    <div className='max-w-5xl mx-auto'>
      <h2><a href={post.url} className='hover:text-blue-800'>{post.title}</a></h2>
      <time className='px-2' dateTime={post.pubDate}>{formatDate(post.pubDate)}</time>
      <p className='description'>
        <span dangerouslySetInnerHTML={{__html: post.excerpt}}></span>
        <a className='link italic' href={post.url} aria-label={`Read ${post.title}`}>Read More</a>
      </p>
    </div>
  </article>;
};

export default PostExcerpt;
