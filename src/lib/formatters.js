import dayjs from 'dayjs';
import advancedFormat from 'dayjs/plugin/advancedFormat';

dayjs.extend(advancedFormat);

export const dateForm = dt => {
  return dayjs(dt).format('MMMM Do, YYYY');
};
