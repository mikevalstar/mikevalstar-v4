module.exports = {
  mode: 'jit',
  darkMode: 'class',
  purge: ['./public/**/*.html', './src/**/*.{astro,js,jsx,ts,tsx,vue,svelte}'],
};
